<?php

// Vérification que login et passwd sont correct
if (!isset($_POST['login']) || !isset($_POST['passwd']) || empty($_POST['passwd']) || empty($_POST['login']) || $_POST['submit'] != 'OK') {
    echo "ERROR\n";

    return;
}

// Création  d'un chemin de stockage des login et mdp
$file = '../ex21/private/passwd';

// Création  d'un fichier de stockage des login et mdp si le fichier n'existe pas
if (!file_exists($file)) {
    mkdir('../ex21/private', 0777);
}

// Création d'un tableau de mdp cryptés
$login = ['login' => $_POST['login'], 'passwd' => password_hash($_POST['passwd'], PASSWORD_DEFAULT)];

// Vérification qu'il n'y a pas de doublons
$users = file_get_contents($file);
$users = unserialize($users);
foreach ($users as $user) {
    if ($user['login'] == $_POST['login']) {
        echo "ERROR\n";

        return;
    }
}

// ajout login
$users[] = $login;
$serial = serialize($users);

if (!file_put_contents($file, $serial)) {
    echo "ERROR\n";

    return;
}

echo "OK\n";
