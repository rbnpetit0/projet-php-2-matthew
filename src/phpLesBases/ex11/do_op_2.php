<?php

if ($argc == 2) {
    if (isset($argv[1])) {
        preg_match_all('/-?[0-9]+\.[0-9]+|\-?[0-9]+/', $argv[1], $match);
        $matchops = preg_split('/-?[0-9]+\.[0-9]+|\-?[0-9]+|\s/', $argv[1], -1, PREG_SPLIT_NO_EMPTY);

        // Vérification que les input sont biens des nombres
        if (!is_numeric($match[0][0]) || !is_numeric($match[0][1])) {
            echo 'Syntax Error' . "\n";
            exit();
        }

        $var1 = $match[0][0];
        $operator = $matchops[0];
        $var2 = $match[0][1];

        switch ($operator) {
        case '+':
        echo $var1 + $var2 . "\n";
        break;

        case '-':
        echo $var1 - $var2 . "\n";
        break;

        case '*':
        echo $var1 * $var2 . "\n";
        break;

        case '/':
            if ($var2 == 0) {
                echo '0' . "\n";
                break;
            } else {
                echo $var1 / $var2 . "\n";
                break;
            }

            // no break
        case '%':
        echo abs(fmod($var1, $var2)) . "\n";
        break;

        default: echo 'Syntax Error' . "\n";
                    break;
        }
    } else {
        echo 'Incorrect Parameters' . "\n";
        exit();
    }
} else {
    echo 'Incorrect Parameters' . "\n";
    exit();
}
