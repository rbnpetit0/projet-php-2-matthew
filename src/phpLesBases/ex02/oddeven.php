<?php

echo 'Entrez un nombre: ';
// Boucle infini qui relance le script indéfiniment
while (true) {
    //  demande d'un input à l'utilisateur
    $var = trim(fgets(STDIN));

    // si le reste de la division par 2 = 0 alors le chiffre est pair
    if (is_numeric($var)) {
        if ($var % 2 == 0) {
            echo "Le chiffre $var est Pair\nEntrez un nombre: ";
        } else {
            echo "Le chiffre $var est Impair\nEntrez un nombre: ";
        }
    } else {
        echo "'$var' n'est pas un chiffre\nEntrez un nombre: ";
    }
}
