<?php

$string = trim(implode(' ', array_slice($argv, 1)));
$arr1 = preg_split("/\s/", $string);

$arr_numbers = [];
$arr_words = [];
$arr_special = [];

foreach ($arr1 as $value) {
    if (is_numeric($value)) {
        array_push($arr_numbers, $value);
        sort($arr_numbers, SORT_STRING);
    } elseif (ctype_alpha($value)) {
        array_push($arr_words, $value);
        natcasesort($arr_words);
    } else {
        array_push($arr_special, $value);
        sort($arr_special, SORT_STRING);
    }
}

$result = array_merge($arr_words, $arr_numbers, $arr_special);

echo implode("\n", $result) . "\n";
