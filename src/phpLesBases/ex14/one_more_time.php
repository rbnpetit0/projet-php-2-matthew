<?php

$WFORMAT = 'Wrong Format';

// Vérification qu'il n'y a qu'un seul parametre
if ($argc !== 2) {
    echo $WFORMAT . "\n";
    exit();
}

// Vérification que le parametre n'est pas null
if (empty($argv[1])) {
    exit();
}

$strdate = strtolower($argv[1]);

$arr_M = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
$arr_m = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
$newstr = str_replace($arr_M, $arr_m, $strdate);

// Vérification que le parametre est correct
$arr1 = explode(' ', $newstr);
if (count($arr1) !== 5) {
    echo $WFORMAT . "\n";
    exit();
}

// Vérification que la date est correct
$month = intval($arr1[2]);
$day = intval($arr1[1]);
$year = intval($arr1[3]);

if (checkdate($month, $day, $year) == false) {
    echo $WFORMAT . "\n";
    exit();
}

// Vérification que l'heure est correct
function check_24_timeFormat($time)
{
    if (preg_match('#((0([0-9])|(1[0-9]{1})|(2[0-3])):([0-5])([0-9]):([0-5])([0-9]))#', $time)) {
        return true;
    } else {
        return false;
    }
}

if (check_24_timeFormat($arr1[4]) == false) {
    echo $WFORMAT . "\n";
    exit();
}

 $datetime = $strdate . ' heure normale d’Europe centrale';

$parser = new IntlDateFormatter(
    'fr_FR',
    IntlDateFormatter::FULL,
    IntlDateFormatter::FULL
);
$result = $parser->parse($datetime);

// Vérification que le parametre est bien ortographié
if ($result == false) {
    echo $WFORMAT . "\n";
    exit();
} else {
    echo $result . "\n";
}
  // $datetime = 'mardi 12 novembre 2013 12:02:21 heure normale d’Europe centrale';
