<?php

namespace App\POO\Ex04;

use App\Resources\Classes\Lannister\Lannister;

class Jaime extends Lannister
{
    public function sleepWith($censored)
    {
        if (is_subclass_of($censored, 'App\Resources\Classes\Lannister\Lannister')) {
            if (is_a($censored, 'App\Resources\Classes\Lannister\Cersei')) {
                echo "With pleasure, but only in a tower in Winterfell, then.\n";
            } else {
                echo "Not even if I'm drunk !\n";
            }
        } else {
            echo "Let's do this.\n";
        }
    }
}
