<?php

namespace App\POO\Ex05;

class NightsWatch implements IFighter
{
    public $soldats = [];

    public function recruit($man)
    {
        $this->soldats[] = $man;
    }

    public function fight()
    {
        foreach ($this->soldats as $man) {
            if ($man instanceof IFighter) {
                $man->fight();
            }
        }
    }
}
