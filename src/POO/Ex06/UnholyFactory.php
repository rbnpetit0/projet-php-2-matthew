<?php

namespace App\POO\Ex06;

class UnholyFactory
{
    public $army = [];

    public function absorb($recruit)
    {
        if (is_a($recruit, 'App\POO\Ex06\Fighter')) {
            if (in_array($recruit, $this->army)) {
                echo "(Factory already absorbed a fighter of type {$recruit->soldier_type})\n";
            } else {
                $this->army[] = $recruit;
                echo "(Factory absorbed a fighter of type {$recruit->soldier_type})\n";
            }
        } else {
            echo "(Factory can't absorb this, it's not a fighter)\n";
        }

        return $this;
    }

    public function fabricate($recruit)
    {
        foreach ($this->army as $value) {
            if ($value->soldier_type == $recruit) {
                echo "(Factory fabricates a fighter of type {$recruit})\n";

                return $value;
            }
        }

        echo "(Factory hasn't absorbed any fighter of type $recruit)\n";
    }
}
